﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour {

	public float delay = 3f;

	void Update ()
	{
		Destroy (gameObject, delay);
	}
}
