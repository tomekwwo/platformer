﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MyButton : MonoBehaviour {

    public Sprite pressedTexture;
    public Sprite releasedTexture;

    Image buttonImage;

    void Start()
    {
        buttonImage = GetComponent<Image>();
    }

    public void PushButton()
    {
        buttonImage.sprite = pressedTexture;
    }

    public void ReleaseButton()
    {
        buttonImage.sprite = releasedTexture;
    }
}
