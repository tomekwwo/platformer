﻿using UnityEngine;

[RequireComponent (typeof(RectTransform))]
public class MoveUI : MonoBehaviour {

    RectTransform rect;
    bool move = false;
    Touch touch;

	void Start ()
    {
        rect = GetComponent<RectTransform> ();
    }

    void Update ()
    {
        if (move)
        {
            rect.position = Input.mousePosition;
        }
    }

    public void BeginMove ()
    {
        move = true;

        //if (Application.platform == RuntimePlatform.Android)
        //{
            
        //}
    }

    public void EndMove ()
    {
        move = false;
    }
}
