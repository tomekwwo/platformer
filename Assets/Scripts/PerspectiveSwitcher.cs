﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (MatrixBlender))]
public class PerspectiveSwitcher : MonoBehaviour
{
    private Matrix4x4 ortho,
                        perspective;
    public float fov = 60f,
                        near = .3f,
                        far = 1000f,
                        orthographicSize = 50f;
    private float aspect;
    private MatrixBlender blender;
    private bool orthoOn;

    public float switchDuration = 1;
    public bool isSwitching = false;

    void Start ()
    {
        aspect = (float) Screen.width / (float) Screen.height;
        ortho = Matrix4x4.Ortho (-orthographicSize * aspect, orthographicSize * aspect, -orthographicSize, orthographicSize, near, far);
        perspective = Matrix4x4.Perspective (fov, aspect, near, far);
        camera.projectionMatrix = ortho;
        orthoOn = true;
        blender = (MatrixBlender) GetComponent (typeof (MatrixBlender));
    }

    public void Switch ()
    {
        orthoOn = !orthoOn;
        if (orthoOn)
            blender.BlendToMatrix (ortho, 1f);
        else
            blender.BlendToMatrix (perspective, 1f);
    }

    public void Switch (bool orthographic, float duration)
    {
        if (orthographic)
        {
            orthoOn = true;
            if (duration == 0.0f)
                blender.BlendToMatrix (ortho, switchDuration);
            else
                blender.BlendToMatrix (ortho, duration);
        }
        else
        {
            orthoOn = false;
            if (duration == 0.0f)
                blender.BlendToMatrix (perspective, switchDuration);
            else
                blender.BlendToMatrix (perspective, duration);
        }
    }

    public IEnumerator Switch (bool orthographic, float time, float duration)
    {
        isSwitching = true;
        Switch (orthographic, duration);

        yield return new WaitForSeconds (time);

        Switch (!orthographic, duration);
        isSwitching = false;
    }
}