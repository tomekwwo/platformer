﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Analog : MonoBehaviour {

    public static float horizontal = 0;
    public static float vertical = 0;
    public static int analogID = -1;

    public float maxDistance = 100;

    Image analogBaseImage, analogHandleImage;
    Touch analogTouch;
    bool analogActive = false;

    void Start ()
    {
        analogBaseImage = GetComponent<Image> ();
        analogHandleImage = GameObject.Find (name + "/AnalogHandle").GetComponent<Image> ();
        analogBaseImage.enabled = true;
        analogHandleImage.enabled = true;
    }

    void Update ()
    {
        if (Input.touchCount > 0 && analogActive)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.position.x < (Screen.width / 2))
                {
                    analogTouch = Input.GetTouch (touch.fingerId);
                    break;
                }
            }

            if (analogTouch.phase == TouchPhase.Moved || analogTouch.phase == TouchPhase.Stationary)
            {
                analogHandleImage.transform.position = transform.position + Vector3.ClampMagnitude (analogTouch.position - (Vector2) transform.position, maxDistance);
                horizontal = analogHandleImage.transform.position.x - transform.position.x;
                vertical = analogHandleImage.transform.position.y - transform.position.y;

                // Normalization
                horizontal /= maxDistance;
                vertical /= maxDistance;
            }

            if (analogTouch.phase == TouchPhase.Ended)
                analogID = -1;
        }
    }

    public void ActivateAnalog ()
    {
        analogActive = true;
    }

    public void DeactivateAnalog ()
    {
        analogActive = false;
        analogHandleImage.transform.position = transform.position;
        horizontal = 0;
        vertical = 0;
    }
}
