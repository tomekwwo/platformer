﻿using UnityEngine;
using System.Collections;

public class PlayerTPP : MonoBehaviour {

    public float lerpSpeed = 10;
    public float moveSpeed = 6;
    public float turnSpeed = 90;
    public float height = 1;
    public GameObject playerParticles;

    Vector3 myNormal;
    Vector3 surfaceNormal;
    Vector3 moveVector;
    Vector3 rotationVector;
    ParticleSystem particles;
    PCControls input;
    float moveVert;
    float moveHor;
    bool canMove = true;

    void Start ()
    {
        myNormal = transform.up;

        if (playerParticles != null)
            particles = playerParticles.GetComponent<ParticleSystem> ();
        else
        {
            Debug.Log ("Couldn't find player particles");
            Debug.Break ();
        }

        if (Application.platform != RuntimePlatform.Android)
        {
            input = GameObject.Find ("PCControls").GetComponent<PCControls> ();
            if (input == null)
            {
                Debug.Log (name + " couldn't find controls!");
                Debug.Break ();
            }
        }
    }

    void Update ()
    {
        if (input != null)
        {
            moveVert = input.GetVertical ();
            moveHor = input.GetHorizontal ();
        }
        else
        {
            moveVert = Analog.vertical;
            moveHor = Analog.horizontal;
        }

        Ray ray;
        RaycastHit hit;

        // update4 surface normal
        ray = new Ray (transform.position, -transform.up);
        if (Physics.Raycast (ray, out hit))
        {
            surfaceNormal = hit.normal;
        }
        else
            surfaceNormal = transform.up;

        myNormal = Vector3.Lerp (myNormal, surfaceNormal, lerpSpeed * Time.deltaTime);
        Vector3 myForward = Vector3.Cross (transform.right, myNormal);

        // align character to the new myNormal while keeping the forward direction
        Quaternion targetRot = Quaternion.LookRotation (myForward, myNormal);
        transform.rotation = Quaternion.Lerp (transform.rotation, targetRot, lerpSpeed * Time.deltaTime);

        // set movement and rotation vectors
        moveVector = Vector3.forward * moveVert;
        rotationVector = new Vector3 (0, moveHor, 0);
    }

    void FixedUpdate ()
    {
        Ray ray = new Ray (transform.position, -transform.up);
        RaycastHit hit;

        // if ray hits something add forces to set proper height
        if (Physics.Raycast (ray, out hit))
        {
            if (hit.distance > height)
                rigidbody.AddRelativeForce (rigidbody.mass * -myNormal);
            else if (hit.distance < height)
                rigidbody.AddRelativeForce (rigidbody.mass * myNormal);
        }

        if (canMove)
        {
            // apply movement force and rotation
            rigidbody.AddRelativeForce (moveVector * moveSpeed * 5);
            transform.Rotate (rotationVector * turnSpeed * Time.deltaTime);
        }
    }

    public void SetMoveAbility (bool value)
    {
        canMove = value;
    }

    void OnCollisionEnter (Collision other)
    {
        // Collision particle effect
        particles.Play ();
    }
}
