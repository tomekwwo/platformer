﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

    public float speed = 5f;
    public Transform target;
    public bool smooth = false;

    void Start ()
    {
        if (target == null)
            target = GameObject.FindWithTag ("Player").GetComponent<Transform> ();
        if (target == null)
            Debug.Log ("Camera has no target!");
    }

    void Update ()
    {
        if(target != null)
            if (smooth)
            {
                transform.position = Vector3.Lerp (transform.position, target.position, Time.deltaTime * speed);
                transform.rotation = Quaternion.Slerp (transform.rotation, target.rotation, Time.deltaTime * speed);
            }
            else
            {
                transform.position = target.position;
                transform.rotation = target.rotation;
            }
    }
}
