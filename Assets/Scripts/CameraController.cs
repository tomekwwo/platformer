﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    TPPCamera tppCamera;
    SideCamera sideCamera;
    Vector3 cameraPos;
    Transform target;

    void Start ()
    {
        tppCamera = GetComponent<TPPCamera> ();
        sideCamera = GetComponent<SideCamera> ();
        target = transform.parent;

        if(tppCamera == null || sideCamera == null)
        {
            Debug.Log ("Needed camera scripts aren't attached!");
        }
    }

    public void SetTPPCamera ()
    {
        camera.isOrthoGraphic = false;
        sideCamera.enabled = false;
        tppCamera.enabled = true;
        cameraPos = new Vector3 (target.position.x, tppCamera.height, -tppCamera.distance);
        transform.localPosition = cameraPos;
        transform.localRotation = target.rotation;
        transform.LookAt (target);
    }

    public void SetSideCamera ()
    {
        tppCamera.enabled = false;
        sideCamera.enabled = true;
        camera.isOrthoGraphic = true;
        cameraPos = new Vector3 (sideCamera.distance, sideCamera.height, 0);
        transform.localPosition = cameraPos;
        transform.localRotation = target.rotation;
        transform.LookAt (target);
    }

    public void ToggleCamera ()
    {
        if (tppCamera.enabled)
            SetSideCamera ();
        else
            SetTPPCamera ();
    }
}
