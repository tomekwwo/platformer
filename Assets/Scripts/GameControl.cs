﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class GameData
{
    public int lastWorld;
    public int lastLevel;
    public float controlsOpacity;
    public bool music = true;
    public bool fx= true;
}

public class GameControl : MonoBehaviour {
    
    public static GameData gameData = new GameData ();
    public static GameControl control;

    public static float gameTime = 0;

    GameObject[] UIControls;

    void Awake ()
    {
        if (control == null)
        {
            DontDestroyOnLoad (gameObject);
            control = this;
        }
        else if (control != null)
        {
            Destroy (gameObject);
        }

        Load ();

        FindUIControls ();
        RefreshUIOpacity ();
    }

    void Update ()
    {
        gameTime += Time.deltaTime;

        if (Application.loadedLevel == 0 && Input.GetKey (KeyCode.Escape))
        {
            Application.Quit ();
        }
    }

    public static void Save ()
    {
        BinaryFormatter bf = new BinaryFormatter ();
        FileStream file = File.Create (Application.persistentDataPath + "/GameData.dat");

        bf.Serialize (file, gameData);
        file.Close ();
    }

    public static void Load ()
    {
        if (File.Exists (Application.persistentDataPath + "/GameData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter ();
            FileStream file = File.Open (Application.persistentDataPath + "/GameData.dat", FileMode.Open);

            gameData = (GameData) bf.Deserialize (file);
            file.Close ();
        }
        else
        {
            gameData.lastWorld = 1;
            gameData.lastLevel = 1;
            gameData.controlsOpacity = 128;
            Save ();
        }
    }

    public static float GetGameTime ()
    {
        gameTime = Mathf.Round (gameTime * 100) / 100;
        
        return gameTime;
    }

    public void SetOpacity (float value)
    {
        gameData.controlsOpacity = value;
        
        RefreshUIOpacity ();
    }

    public void FindUIControls ()
    {
        UIControls = GameObject.FindGameObjectsWithTag ("Controls");
    }

    public void RefreshUIOpacity ()
    {
        foreach (GameObject g in UIControls)
        {
            Image i = g.GetComponent<Image> ();
            i.color = new Color (i.color.r, i.color.g, i.color.b, gameData.controlsOpacity);
        }
    }

    public void ToggleSound (string sound)
    {
        if (sound == "music")
            gameData.music = !gameData.music;
        else if (sound == "fx")
            gameData.fx = !gameData.fx;

        Debug.Log ("Music: " + gameData.music + " FX: " + gameData.fx);
    }
}