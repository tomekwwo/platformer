﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float moveSpeed = 6; // move speed
    public float turnSpeed = 90; // turning speed (degrees/second)
    public float lerpSpeed = 10; // smoothing speed
    public float gravity = 10; // gravity acceleration
    public float height = 1;  // player height over the ground
    public float errorMargin = 0.1f;  // error margin for height
    public GameObject playerParticles;  // player particles object
    public bool moveSideways = false;   // determines movement proper to the camera (TPP or side)

    Vector3 surfaceNormal; // current surface normal
    Vector3 myNormal; // character normal
    Vector3 moveVector;
    Vector3 rotationVector;
    bool canMove = true;
    float moveHor;
    float moveVert;
    ParticleSystem particles;   // player particles that are refreshed after collision
    PCControls input;

    void Start()
    {
        myNormal = transform.up; // normal starts as character up direction 
        rigidbody.freezeRotation = true; // disable physics rotation

        if (playerParticles != null)
            particles = playerParticles.GetComponent<ParticleSystem> ();
        else
        {
            Debug.Log ("Couldn't find player particles");
            Debug.Break ();
        }

        input = GameObject.Find ("PCControls").GetComponent<PCControls> ();
    }

    void FixedUpdate()
    {
        //Ray ray = new Ray (transform.position, -myNormal);
        Ray ray = new Ray (transform.position, -transform.up);
        RaycastHit hit;

        // if ray hits something add forces to set proper height
        if (Physics.Raycast (ray, out hit))
        {
            if (hit.distance > height + errorMargin)
                rigidbody.AddRelativeForce (gravity * rigidbody.mass * -myNormal);
            else if (hit.distance < height - errorMargin)
                rigidbody.AddRelativeForce (gravity * rigidbody.mass * myNormal);
        }

        if(canMove)
        {
            // apply movement force and rotation
            rigidbody.AddRelativeForce (moveVector * moveSpeed * 5);
            if(!moveSideways)
            {
                transform.Rotate (rotationVector * turnSpeed * Time.deltaTime);
            }
        }
    }

    void Update()
    {
        moveVert = input.GetVertical ();
        moveHor = input.GetHorizontal ();

        Ray ray;
        RaycastHit hit;

        // update surface normal
        ray = new Ray(transform.position, -myNormal); // cast ray downwards
        if (Physics.Raycast (ray, out hit)) // use it to update myNormal
        {
            surfaceNormal = hit.normal;
        }
        else
        {
            // assume usual ground normal to avoid "falling forever"
            //surfaceNormal = Vector3.up;
            surfaceNormal = transform.up;
        }

        myNormal = Vector3.Lerp (myNormal, surfaceNormal, lerpSpeed * Time.deltaTime);

        // find forward direction with new myNormal
        Vector3 myForward = Vector3.Cross(transform.right, myNormal);

        // align character to the new myNormal while keeping the forward direction
        if (!moveSideways)
        {
            Quaternion targetRot = Quaternion.LookRotation (myForward, myNormal);
            transform.rotation = Quaternion.Lerp (transform.rotation, targetRot, lerpSpeed * Time.deltaTime);
        }
        
        // set movement and rotation vectors
        if(!moveSideways)
        {
            moveVector = Vector3.forward * moveVert;
            rotationVector = new Vector3 (0, moveHor, 0);
        }
        else
        {
            moveVector = new Vector3 (-moveVert, 0, moveHor);
        }
    }

    public void SetMoveAbility (bool value)
    {
        canMove = value;
    }

    public void InputHorizontal (float value)
    {
        moveHor = value;
    }

    public void InputVertical (float value)
    {
        moveVert = value;
    }

    void OnCollisionEnter (Collision other)
    {
        // Collision particle effect
        if(other.gameObject.tag != "Map")
            particles.Play ();
    }
}