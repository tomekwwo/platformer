﻿using UnityEngine;
using System.Collections;

public class RGBCubeCollision : MonoBehaviour {

    GameObject socketObject;
    RGBSocket socket;

    void Start()
    {
        socketObject = transform.parent.gameObject;
        socket = socketObject.GetComponent<RGBSocket>();
    }

	void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            socket.CubeActive(true);
        }
    }
}
