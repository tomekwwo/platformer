﻿using UnityEngine;
using System.Collections;

public class RGBSocket : MonoBehaviour {

    public Color inactiveColor;
    public Color activeColor;
    [HideInInspector]
    public bool socketActive = false;
    [HideInInspector]
    public bool cubeActive = false;

    GameObject cube;
    GameObject[] sockets;
    GameObject socLight;
    bool mounted = false;
    Transform mountPoint;
    RGBSocket rgbSock;
    RGBRiddleController riddleController;

    void Start()
    {
        cube = GameObject.Find(name + "/Cube");
        cube.renderer.material.color = inactiveColor;
        mountPoint = GameObject.Find(name + "/MountPoint").GetComponent<Transform>();
        sockets = GameObject.FindGameObjectsWithTag("RGBSocket");
        socLight = GameObject.Find(name + "Light");
        riddleController = transform.parent.gameObject.GetComponent<RGBRiddleController>();
        if (riddleController == null)
            Debug.Log(name + " couldn't find RGBRiddleController");

        socLight.SetActive(false);
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player")
        {
            foreach (GameObject sock in sockets)
            {
                if (sock.name != name)
                {
                    rgbSock = sock.GetComponent<RGBSocket>();
                    if (rgbSock == null)
                    {
                        Debug.Log(name + " couldn't find " + sock.name + " socket");
                    }
                    else
                    {
                        if (!rgbSock.IsMounted() && rgbSock.socketActive)
                        {
                            rgbSock.socketActive = false;
                            rgbSock.socLight.SetActive(false);
                        }
                    }
                }
            }

            socLight.SetActive(true);
            socketActive = true;

            Check();
        }
    }

    public void CubeActive(bool value)
    {
        if(value)
        {
            foreach(GameObject sock in sockets)
            {
                if(sock.name != name)
                {
                    rgbSock = sock.GetComponent<RGBSocket>();
                    if(rgbSock == null)
                    {
                        Debug.Log(name + " couldn't find " + rgbSock.name + " socket");
                    }
                    else
                    {
                        if(rgbSock.cubeActive && !rgbSock.mounted)
                        {
                            rgbSock.cubeActive = false;
                            rgbSock.cube.renderer.material.color = rgbSock.inactiveColor;
                        }
                    }
                }
            }
        }

        cubeActive = value;

        if (cubeActive)
            cube.renderer.material.color = activeColor;
        else
            cube.renderer.material.color = inactiveColor;

        Check();
    }

    public bool IsMounted()
    {
        return mounted;
    }

    void Check()
    {
        if (socketActive && cubeActive)
            Mount();
    }

    void Mount()
    {
        cube.transform.position = mountPoint.position;
        socLight.GetComponent<Light>().color = activeColor;
        socLight.GetComponent<Light>().intensity = 0.5f;
        mounted = true;

        riddleController.Check();
    }
}
