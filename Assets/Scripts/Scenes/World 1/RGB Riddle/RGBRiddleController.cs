﻿using UnityEngine;
using System.Collections;

public class RGBRiddleController : MonoBehaviour {

    [HideInInspector]
    public int socketsMounted;

    GameObject[] sockets;
    bool allMounted = false;

    void Start()
    {
        sockets = GameObject.FindGameObjectsWithTag("RGBSocket");
    }

    void Update()
    {
        // Do while mechanism is triggered
        if(allMounted)
        {
            GameObject.Find("Test").transform.Rotate(Vector3.forward * Time.deltaTime * 30);
        }
    }

    public void Check()
    {
        socketsMounted = 0;
        if (!allMounted)
        {
            foreach (GameObject sock in sockets)
            {
                if (sock.GetComponent<RGBSocket>().IsMounted())
                    socketsMounted++;
            }
            
            if (socketsMounted == sockets.Length)
            {
                allMounted = true;
            }
        }
    }
}
