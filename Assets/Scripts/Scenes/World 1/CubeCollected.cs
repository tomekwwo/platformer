﻿using UnityEngine;
using System.Collections;

public class CubeCollected : MonoBehaviour {

    public float lerpSpeed = 2;
    public Color initColor;
    public Color specColor;
    public GameObject target;

    bool cubeCollected = false;
    Color zeroAlpha;
    Collider col;

    void Start ()
    {
        zeroAlpha = initColor;
        zeroAlpha.a = 0;
        col = GetComponent<Collider> ();
        renderer.material.color = initColor;
        renderer.material.SetColor ("_SpecColor", specColor);

        if (target == null)
        {
            target = new GameObject ();
            target.transform.position = transform.up * 2;
        }
    }

    void Update ()
    {
        if(cubeCollected)
        {
            transform.position = Vector3.Lerp (transform.position, target.transform.position, Time.deltaTime * lerpSpeed);
            renderer.material.color = Color.Lerp (renderer.material.color, zeroAlpha, Time.deltaTime * lerpSpeed);

            if(renderer.material.color.a <= 0.01f)
            {
                target.SetActive (true);
                Destroy (gameObject);
            }
        }
    }

	void OnTriggerEnter (Collider other)
    {
        if(other.tag == "Player")
        {
            cubeCollected = true;
            col.enabled = false;
        }
    }
}
