﻿using UnityEngine;
using System.Collections;

public class Delay : MonoBehaviour {

    public float delay = 3f;
    public string levelName;

    float timer = 0f;

    void Update()
    {
        timer += Time.deltaTime;

        if(timer >= delay)
        {
            Application.LoadLevel(levelName);
        }
    }
}
