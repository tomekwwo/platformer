﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Slider))]
public class OpacitySlider : MonoBehaviour {

    Slider slider;
    GameControl control;

    void OnEnable ()
    {
        slider = GetComponent<Slider> ();

        control = GameObject.Find ("GameControl").GetComponent<GameControl> ();
        if (control == null)
            Debug.Log (name + " can't find GameControl");
        else
            control.FindUIControls ();

        slider.value = GameControl.gameData.controlsOpacity;
    }

    void OnDisable ()
    {
        GameControl.Save ();
    }

    public void SetValue ()
    {
        control.SetOpacity (slider.value);
    }
}
