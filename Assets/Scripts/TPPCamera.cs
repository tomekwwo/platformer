﻿using UnityEngine;
using System.Collections;

public class TPPCamera : MonoBehaviour {

    public float height = 2;
    public float distance = 4;
    public float speed = 5;
    public float obstacleDistance = 2.5f;

    Transform target;
    bool isColliding = false;
    bool closeObstacle = false;

    void Start ()
    {
        target = transform.parent;
        transform.position = new Vector3 (target.position.x, target.position.y + height, target.position.z - distance);

        camera.isOrthoGraphic = false;
    }

    void FixedUpdate ()
    {
        Ray ray = new Ray (transform.position, -transform.forward);
        RaycastHit hit;

        if (Physics.Raycast (ray, out hit))
        {
            if (hit.distance <= obstacleDistance)
                closeObstacle = true;
            else
                closeObstacle = false;
        }

        if (!isColliding && !closeObstacle)
        {
            //transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, Mathf.SmoothDamp (transform.localPosition.z, -distance, ref currVelocity, Time.deltaTime * speed));
            transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, Mathf.SmoothStep (transform.localPosition.z, -distance, Time.deltaTime * speed));
        }

        transform.LookAt (target);
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.tag != "Player")
            isColliding = true;
    }

    void OnTriggerStay (Collider other)
    {
        if (other.tag != "Player")
        {
            //transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, Mathf.SmoothDamp (transform.localPosition.z, 0, ref currVelocity, Time.deltaTime * speed));
            transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, Mathf.SmoothStep (transform.localPosition.z, 0, Time.deltaTime * speed));
        }
    }

    void OnTriggerExit (Collider other)
    {
        if (other.tag != "Player")
            isColliding = false;
    }
}
