﻿using UnityEngine;

public class ToggleSound : MonoBehaviour {

    GameControl control;

    void OnEnable ()
    {
        control = GameObject.Find ("GameControl").GetComponent<GameControl> ();
    }

    public void Toggle (string sound)
    {
        control.ToggleSound (sound);
    }
}
