﻿using UnityEngine;

public enum Swipe { None, Up, UpRight, Right, DownRight, Down, DownLeft, Left, UpLeft };

public class SwipeManager : MonoBehaviour {

    public static Swipe swipeDirection;

    public float minSwipeLength = 100f;
    
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    Touch swipeTouch;
    int touchID = 0;
    int swipeFrames = 0;

    void Update ()
    {
        // Check if current swipe exists only for one frame
        if (swipeFrames == 1 && swipeDirection != Swipe.None)
        {
            Debug.Log ("Swipe: " + swipeDirection);
            swipeFrames++;
        }
        else
        {
            swipeFrames = 0;
            swipeDirection = Swipe.None;
        }
    }

    public void BeginSwipe ()
    {
        if (Application.platform != RuntimePlatform.Android)
        {
            // If it's not Android check mouse position
            if (Input.mousePosition.x > Screen.width / 2)
                firstPressPos = Input.mousePosition;
            else
                firstPressPos = new Vector2 (-1, -1);
        }
        else
        {
            // If it is Android check touches
            if (Input.touchCount > 0)
            {
                foreach (Touch touch in Input.touches)
                {
                    // Identify swipe touch
                    if (touch.position.x > (Screen.width / 2))
                    {
                        touchID = touch.fingerId;
                        swipeTouch = Input.GetTouch (touch.fingerId);
                        Debug.Log ("ID: " + touch.fingerId);
                        break;
                    }
                }
            }
            
            if (swipeTouch.position.x > (Screen.width / 2))
                firstPressPos = swipeTouch.position;
            else
                firstPressPos = new Vector2 (-1, -1);
        }
    }

    public void EndSwipe ()
    {
        if (Application.platform != RuntimePlatform.Android)
        {
            // If it's not Android check mouse position
            secondPressPos = Input.mousePosition;
            currentSwipe = new Vector2 (secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
        }
        else
        {
            // If it is Android check touch position
            swipeTouch = Input.GetTouch (touchID);
            secondPressPos = swipeTouch.position;
            currentSwipe = new Vector3 (secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
        }

        // Check if it is a swipe
        if (currentSwipe.magnitude < minSwipeLength)
        {
            swipeDirection = Swipe.None;
            return;
        }

        currentSwipe.Normalize ();

        if (firstPressPos.x != -1)
        {
            if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)   // Swipe up
            {
                swipeDirection = Swipe.Up;
                return;
            }
            else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)  // Swipe down
            {
                swipeDirection = Swipe.Down;
                return;
            }
            else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)  // Swipe left
            {
                swipeDirection = Swipe.Left;
                return;                
            }
            else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)  // Swipe right
            {
                swipeDirection = Swipe.Right;
                return;
            }
            else if (currentSwipe.y > 0 && currentSwipe.x < 0)     // Swipe up left
            {
                swipeDirection = Swipe.UpLeft;
                return;
            }
            else if (currentSwipe.y > 0 && currentSwipe.x > 0)     // Swipe up right
            {
                swipeDirection = Swipe.UpRight;
                return;
            }
            else if (currentSwipe.y < 0 && currentSwipe.x < 0)     // Swipe down left
            {
                swipeDirection = Swipe.DownLeft;
                return;
            }
            else if (currentSwipe.y < 0 && currentSwipe.x > 0)     // Swipe down right
            {
                swipeDirection = Swipe.DownRight;
                return;
            }
        }
        swipeDirection = Swipe.None;
    }
}
