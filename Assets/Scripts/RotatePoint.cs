﻿using UnityEngine;
using System.Collections;

public class RotatePoint : MonoBehaviour {

    public bool auto = false;
    public Vector3 rotation1, rotation2;
	public GameObject particles;
    public bool playParticles = false;

    PlayerSideways playerController;
    GameObject player;
	bool movePlayer = false;
    PerspectiveSwitcher switcher;

	void Start ()
	{
        player = GameObject.FindWithTag("Player");
        if (player != null)
            playerController = player.GetComponent<PlayerSideways>();
        else
            Debug.Log(name + " couldn't find Player");

        switcher = GameObject.FindWithTag ("MainCamera").GetComponent<PerspectiveSwitcher> ();
        if (switcher == null)
        {
            Debug.Log ("Main camera doesn't have perspective switcher!");
            Debug.Break ();
        }
	}

	void Update ()
	{
		if(movePlayer)
		{
			player.rigidbody.velocity = Vector3.zero;
			if(Vector3.Distance (player.transform.position, transform.position) > 0.1f)
			{
				player.transform.position = Vector3.Lerp (player.transform.position, transform.position, 5 * Time.deltaTime);
			}
			else
			{
                player.transform.position = transform.position;
				playerController.SetMoveAbility (true);
				movePlayer = false;
			}
		}
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!CompareVectors (player.transform.rotation.eulerAngles, transform.rotation.eulerAngles, 0.1f) && !switcher.isSwitching)
            {
                StartCoroutine (switcher.Switch (false, 1, 1));
            }

            if (!auto)
                RotatePlayer ();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && auto)
        {
            if (Vector3.Distance(player.transform.rotation.eulerAngles, transform.rotation.eulerAngles) > 10f)
                other.transform.rotation = Quaternion.Lerp(other.transform.rotation, transform.rotation, Time.deltaTime * 3);
            else
                other.transform.rotation = transform.rotation;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player" && auto)
        {
            other.transform.rotation = transform.rotation;
        }
    }

	public void RotatePlayer ()
	{
        if (playParticles)
		    Instantiate (particles, transform.position, transform.rotation);

        if (CompareVectors (player.transform.rotation.eulerAngles, rotation1, 0.1f))
        {
            player.transform.localRotation = Quaternion.Euler (rotation2);
        }
        else
        {
            player.transform.localRotation = Quaternion.Euler (rotation1);
        }
        
		player.rigidbody.velocity = Vector3.zero;
		playerController.SetMoveAbility (false);
		movePlayer = true;
    }

    bool CompareVectors (Vector3 v1, Vector3 v2, float precision)
    {
        if (Mathf.Abs (v1.x - v2.x) > precision)
            return false;
        else if (Mathf.Abs (v1.y - v2.y) > precision)
            return false;
        else if (Mathf.Abs (v1.z - v2.z) > precision)
            return false;
        else
            return true;
    }
}