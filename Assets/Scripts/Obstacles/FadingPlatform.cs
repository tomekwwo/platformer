﻿using UnityEngine;
using System.Collections;

public class FadingPlatform : MonoBehaviour {

	public float delay = 3f;
	public float speed = 1f;

	float timer = 0f;
	Color color;
	bool transfer = false;
	float targetAlpha = 0f;
	bool playerInWay = false;

	void Start ()
	{
		color = renderer.material.color;
	}

	void Update ()
	{
		timer += Time.deltaTime;

		if(timer >= delay && !transfer && !playerInWay)
		{
			transfer = true;

			if(color.a < 0.1f)
				targetAlpha = 1f;
			else
				targetAlpha = 0f;
		}

		if(transfer)
		{
			if(renderer.material.color.a == targetAlpha)
			{
				transfer = false;
				timer = 0f;
			}

			if(color.a < targetAlpha)
			{
				if(color.a + Time.deltaTime > 1f)
				{
					color.a = 1f;
				}
				else
					color.a += Time.deltaTime * speed;
				renderer.material.color = color;
			}
			else if(color.a > targetAlpha)
			{
				if(color.a - Time.deltaTime < 0f)
				{
					color.a = 0f;
				}
				else
					color.a -= Time.deltaTime * speed;
				renderer.material.color = color;
			}
		}

		if(renderer.material.color.a == 0f)
			collider.isTrigger = true;
		else
			collider.isTrigger = false;
	}

	void OnTriggerStay (Collider other)
	{
		if(other.tag == "Player" && collider.isTrigger)
			playerInWay = true;
	}

	void OnTriggerExit (Collider other)
	{
		if(other.tag == "Player")
			playerInWay = false;
	}
}
