﻿using UnityEngine;
using System.Collections;

public class Swiper : MonoBehaviour {

	public Transform obstacle;
	public Transform pointA;
	public Transform pointB;
	public float speed = 6f;
	public float delay = 0f;

	bool MoveToB = false;
	float timer = 0f;

	void Start ()
	{
		obstacle.position = pointA.position;
	}

	void Update ()
	{
		timer += Time.deltaTime;

		if(obstacle.position == pointA.position && timer >= delay)
		{
			MoveToB = true;
			timer = 0f;
		}
		else if(obstacle.position == pointB.position && timer >= delay)
		{
			MoveToB = false;
			timer = 0f;
		}
	}

	void FixedUpdate ()
	{
		if(MoveToB)
		{
			obstacle.position = Vector3.Lerp (obstacle.position, pointB.position, Time.deltaTime * speed);
		}
		else
		{
			obstacle.position = Vector3.Lerp (obstacle.position, pointA.position, Time.deltaTime * speed);
		}
	}
}
