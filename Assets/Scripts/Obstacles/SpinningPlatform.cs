﻿using UnityEngine;
using System.Collections;

public class SpinningPlatform : MonoBehaviour {

	public float speed = 5f;
	public float delay = 3f;
	public float defTilt = 0f;
	public float tilt = 180f;

	float timer = 0f;
	Quaternion targetRotation;
	bool rotateToTarget = true;

	void Start ()
	{
		transform.rotation = Quaternion.Euler (0, 0, defTilt);
		targetRotation = Quaternion.Euler (0, 0, tilt);
	}

	void Update ()
	{
		timer += Time.deltaTime;

		if(timer >= delay)
		{
			if(targetRotation == Quaternion.Euler (0, 0, defTilt))
				targetRotation = Quaternion.Euler (0, 0, tilt);
			else if(targetRotation == Quaternion.Euler (0, 0, tilt))
				targetRotation = Quaternion.Euler (0, 0, defTilt);

			rotateToTarget = true;
			timer = 0f;
		}
	}

	void FixedUpdate ()
	{
		if(rotateToTarget)
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * speed);
		}
	}
}
