﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour {
    
    public GameObject[] levelSelectionPanels;
    public GameObject worldSelectionPanel;

    GameObject[] buttons;
    string levelName = "";
    int worldSelected = 0;

    void Awake ()
    {
        foreach (GameObject g in levelSelectionPanels)
        {
            g.SetActive (false);
        }

        worldSelectionPanel.SetActive (true);

        CheckLevels (GameControl.gameData.lastWorld);
    }

    void CheckLevels (int levelsPassed)
    {
        buttons = GameObject.FindGameObjectsWithTag ("LevelSelect");
        
        foreach (GameObject i in buttons)
        {
            if (worldSelected == 0 || worldSelected == GameControl.gameData.lastWorld)
            {
                if (i.GetComponent<LoadLevel> ().levelID <= levelsPassed)
                {
                    i.GetComponent<Button> ().interactable = true;
                }
                else
                    i.GetComponent<Button> ().interactable = false;
            }
            else if (worldSelected > 0 && worldSelected < GameControl.gameData.lastWorld)
            {
                i.GetComponent<Button> ().interactable = true;
            }
        }
    }

    public void WorldSelected (int lev)
    {
        worldSelected = lev;
        levelName += "LevelW" + lev + "-";
        worldSelectionPanel.SetActive (false);
        levelSelectionPanels[lev - 1].SetActive (true);
        CheckLevels (GameControl.gameData.lastLevel);
    }

    public void LevelSelected (int lev)
    {
        levelName += lev.ToString ();
        Application.LoadLevel (levelName);
    }
}
