﻿using UnityEngine;

public class ToggleObject : MonoBehaviour
{
    public GameObject targetObject;

    public void Toggle ()
    {
        if (targetObject != null)
        {
            targetObject.SetActive (!targetObject.activeSelf);
        }
        else
            Debug.Log ("There's no object to toggle!");
    }
}
