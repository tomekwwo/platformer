﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

    public int levelID;
    public string levelName;

    public void LoadLevByName ()
    {
        if (levelName != "")
        {
            Application.LoadLevel (levelName);
            GameControl.gameTime = 0;
        }
        else
            Debug.Log (name + " doesn't have name of a level to load!");
    }

    public void LoadLevByID ()
    {
        if (levelID >= 0)
        {
            GameControl.gameTime = 0;
            Application.LoadLevel (levelID);
        }
        else
            Debug.Log (name + " doesn't have ID of a level to load!");
    }

    public void LoadNextLevel ()
    {
        GameControl.gameTime = 0;
        Application.LoadLevel (Application.loadedLevel + 1);
    }

    public void RestartLevel ()
    {
        GameControl.gameTime = 0;
        Application.LoadLevel (Application.loadedLevel);
    }
}
