﻿using UnityEngine;
using System.Collections;

public class SideCamera : MonoBehaviour {

    public float height = 1;
    public float distance = 4;

    Transform target;
    Vector3 wantedPos;

    void Start()
    {
        target = transform.parent;

        wantedPos = new Vector3 (distance, height, 0);
        transform.localPosition = wantedPos;
        transform.LookAt (target);

        camera.isOrthoGraphic = true;
    }
}
