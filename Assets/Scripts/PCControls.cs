﻿using UnityEngine;
using System.Collections;

public class PCControls : MonoBehaviour {

    float moveVert, moveHor;
    
    void Update()
    {
        moveVert = Input.GetAxis("Vertical");
        moveHor = Input.GetAxis("Horizontal");
    }

    public float GetVertical ()
    {
        return moveVert;
    }

    public float GetHorizontal ()
    {
        return moveHor;
    }
}
