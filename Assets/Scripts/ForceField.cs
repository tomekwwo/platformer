﻿using UnityEngine;
using System.Collections;

public class ForceField : MonoBehaviour {

	public float force = 10f;

	void OnTriggerStay (Collider other)
	{
		if(other.tag == "Player")
		{
            other.rigidbody.AddForce(transform.forward * force);
		}
	}
}