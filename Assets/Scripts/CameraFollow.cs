﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public float maxDistance = 5;
    public float minDistance = 3;
    public float height = 2;

    bool colliding = false;
    float speed;
    Transform target;

    void Start()
    {
        target = GameObject.FindWithTag("Player").GetComponent<Transform>();
    }

    void LateUpdate()
    {
        if (!colliding)
        {
            speed = target.rigidbody.velocity.magnitude;
            Debug.Log("V: " + speed);
            if (DistanceFromTarget() > maxDistance)     // If camera is too far from the target
            {
                Vector3 desiredPosition = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * speed);
                desiredPosition.y = target.position.y + height;
                transform.position = desiredPosition;
            }
            else if (DistanceFromTarget() < minDistance)    // If camera is too close to the target
            {
                Vector3 desiredPosition = Vector3.MoveTowards(transform.position, -target.position, Time.deltaTime * speed);
                desiredPosition.y = target.position.y + height;
                transform.position = desiredPosition;
            }
            else    // If camera is between min and max distance from the target
            {
                /*Vector3 desiredPosition = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * Mathf.Lerp(speed, 0, Time.deltaTime));
                desiredPosition.y = target.position.y + height;
                transform.position = desiredPosition;*/
            }
        }

        transform.LookAt(target);
    }

    float DistanceFromTarget()
    {
        float distance = Vector3.Distance(transform.position, target.position);
        distance -= height;
        return distance;
    }

    void OnCollisionEnter(Collision other)
    {
        colliding = true;
    }

    void OnCollisionExit(Collision other)
    {
        colliding = false;
    }

    void OnCollisionStay(Collision other)
    {
        if (DistanceFromTarget() > minDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * speed);
        }
    }
}
