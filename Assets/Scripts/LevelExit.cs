﻿using UnityEngine;
using UnityEngine.UI;

public class LevelExit : MonoBehaviour {
    
    public GameObject levelFinishedPanel;
    public Text gameTimeText;

    bool endLevel = false;
    float timer;
    GameObject player;
    GameObject exitParticles;
    ParticleSystem particles;
    PlayerSideways playerSide;
    PlayerTPP playerTPP;
    float time;
    GameObject menuButton;

    void Start()
    {        
        player = GameObject.FindWithTag("Player");
        if (player != null)
        {
            playerSide = player.GetComponent<PlayerSideways> ();
            if (playerSide == null)
            {
                playerTPP = player.GetComponent<PlayerTPP> ();
                if (playerTPP == null)
                {
                    Debug.Log (name + " couldn't find any player controller!");
                    Debug.Break ();
                }
            }
        }
        else
            Debug.Log (name + " couldn't find Player");

        exitParticles = GameObject.Find (name + "/ExitLevelParticles");

        if (exitParticles == null)
            Debug.Log ("Couldn't find exit level particles");
        else
            particles = exitParticles.GetComponent<ParticleSystem> ();

        if (levelFinishedPanel != null)
        {
            levelFinishedPanel.SetActive (false);
        }
        else
            Debug.Log ("There's no LevelFinishedPanel!");


        menuButton = GameObject.Find ("MenuButton");
        if (menuButton == null)
        {
            Debug.Log (name + " can't find menu button");
        }
    }

    void Update()
    {
        if(endLevel)
        {
            timer += Time.deltaTime;

            player.transform.position = Vector3.Lerp (player.transform.position, transform.position, Time.deltaTime * 5);

            // Disable player visibility
            if (timer >= 1f && player)
                player.SetActive (false);

            if (timer >= 3f)
            {
                if (levelFinishedPanel != null)
                {
                    levelFinishedPanel.SetActive (true);
                    if (gameTimeText != null)
                    {
                        if (time < 60)
                        {
                            gameTimeText.text = "Level passed in\n" + time + " seconds";
                        }
                        else
                        {
                            int minutes = 0;
                            float seconds = time;
                            while (seconds > 60)
                            {
                                seconds -= 60;
                                minutes++;
                            }
                            seconds = Mathf.Round (seconds * 100) / 100;
                            gameTimeText.text = "Level passed in " + minutes + ":" + seconds;
                        }
                    }
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)         // Exit level/world
    {
        if (other.tag == "Player")
        {
            if (playerSide != null)
            {
                playerSide.SetMoveAbility (false);
            }
            else
            {
                playerTPP.SetMoveAbility (false);
            }

            player.rigidbody.isKinematic = true;
            timer = 0f;

            // Enable particle animation
            particles.Play ();
            endLevel = true;
            time = GameControl.GetGameTime ();
            menuButton.SetActive (false);

            if (WorldID () == GameControl.gameData.lastWorld)
            {
                if (LevelID () == GameControl.gameData.lastLevel)
                {
                    if (gameObject.tag == "WorldExit")
                    {
                        GameControl.gameData.lastWorld++;
                        GameControl.gameData.lastLevel = 1;
                    }
                    else
                        GameControl.gameData.lastLevel++;
                    
                    GameControl.Save ();
                }
            }
        }
    }

    int WorldID ()
    {
        string levelName = Application.loadedLevelName;
        string worldID = "";
        bool read = false;

        for (int i = 0; i < levelName.Length; i++)
        {
            if (levelName[i] == '-')
                break;

            if (read)
            {
                worldID += levelName[i];
            }

            if (levelName[i] == 'W')
                read = true;
        }

        int retValue;
        if (int.TryParse (worldID, out retValue))
        {
            return retValue;
        }
        else
        {
            Debug.Log ("Wrong level name! (failed to parse worldID (" + worldID + ")");
            return 0;
        }
    }

    int LevelID ()
    {
        string levelName = Application.loadedLevelName;
        string levelID = "";
        bool read = false;

        for (int i = 0; i < levelName.Length; i++)
        {
            if (read)
            {
                levelID += levelName[i];
            }

            if (levelName[i] == '-')
                read = true;
        }

        int retValue;
        if (int.TryParse (levelID, out retValue))
        {
            return retValue;
        }
        else
        {
            Debug.Log ("Wrong level name! (failed to parse levelID (" + levelID + ")");
            return 0;
        }
    }
}
