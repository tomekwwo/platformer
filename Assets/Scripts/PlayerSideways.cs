﻿using UnityEngine;
using System.Collections;

public class PlayerSideways : MonoBehaviour {

    public float maxSpeed = 7;
    public float moveForce = 15;
    public float height = 1;
    public float heightForce = 5;
    public float dashForce = 1500;
    public float globalCooldown = 1;
    public ParticleSystem playerParticles;

    Vector3 moveVector;
    float moveHor;
    float moveVert;
    float timer = 0;
    bool canMove = true;
    PCControls input;
    float heightCheckRayLength;
    float precision = 0.1f;

    void Start ()
    {
        if (playerParticles == null)
        {
            Debug.Log ("Can't find player particles!");
            Debug.Break ();
        }

        if (Application.platform != RuntimePlatform.Android)
        {
            input = GameObject.Find ("PCControls").GetComponent<PCControls> ();
            if (input == null)
            {
                Debug.Log (name + " couldn't find controls!");
                Debug.Break ();
            }
        }
    }

    void Update ()
    {
        if (input != null)
        {
            moveVert = input.GetVertical ();
            moveHor = input.GetHorizontal ();
        }
        else
        {
            moveVert = Analog.vertical;
            moveHor = Analog.horizontal;
        }

        moveVector = new Vector3 (-moveVert, 0, moveHor);
        
        if (SwipeManager.swipeDirection != Swipe.None && timer == 0)
        {
            if (SwipeManager.swipeDirection == Swipe.Right)
            {
                Dash (transform.forward);
            }
            else if (SwipeManager.swipeDirection == Swipe.Left)
            {
                Dash (-transform.forward);
            }
            timer = globalCooldown;
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
            timer = 0;
    }

    void FixedUpdate ()
    {
        RaycastHit hit;
        heightCheckRayLength = height + 1;
        
        if(Physics.Raycast (transform.position, -transform.up, out hit, heightCheckRayLength))
        {
            // Set proper height over the surface
            if (hit.distance > height + (precision / 2))
            {
                rigidbody.AddForce (-transform.up * heightForce);
            }
            else if (hit.distance < height - (precision / 2))
            {
                rigidbody.AddForce (transform.up * heightForce);
            }
        }
        
        if (canMove)
        {
            // apply movement force
            rigidbody.AddRelativeForce (moveVector * moveForce);
        }
    }

    public void SetMoveAbility (bool value)
    {
        canMove = value;
    }

    void OnCollisionEnter (Collision other)
    {
        // Collision particle effect
        if (other.gameObject.tag != "Map")
            playerParticles.Play ();
    }

    void Dash (Vector3 direction)
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce (direction * dashForce);
        playerParticles.Play ();
    }
}
