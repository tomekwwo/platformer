﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowText : MonoBehaviour {

    Text text;
    bool activated = false;

    void Start ()
    {
        text = GetComponent<Text> ();
        text.color = new Color (text.color.r, text.color.g, text.color.b, 0);
        text.enabled = true;
    }

    void Update ()
    {
        if (activated && text.color.a < 1)
        {
            text.color = new Color (text.color.r, text.color.g, text.color.b, text.color.a + Time.deltaTime);
        }
        else if (!activated && text.color.a > 0)
        {
            text.color = new Color (text.color.r, text.color.g, text.color.b, text.color.a - Time.deltaTime);
        }

        if (text.color.a > 1)
            text.color = new Color (text.color.r, text.color.g, text.color.b, 1);
        else if (text.color.a < 0)
            text.color = new Color (text.color.r, text.color.g, text.color.b, 0);
    }

	public void Show ()
    {
        activated = true;
    }

    public void Hide ()
    {
        activated = false;
    }
}
