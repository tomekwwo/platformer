﻿using UnityEngine;
using System.Collections;

public class EventTrigg : MonoBehaviour {

    public GameObject target;
    public string activateMethod;
    public string deactivateMethod;

	void OnTriggerEnter (Collider other)
    {
        if (activateMethod.Length != 0 && other.tag == "Player")
            target.SendMessage (activateMethod);
        else if (activateMethod.Length == 0)
            Debug.Log ("Event trigger doesn't have method name");
    }

    void OnTriggerExit (Collider other)
    {
        if (deactivateMethod != null && other.tag == "Player")
            target.SendMessage (deactivateMethod);
        else if (deactivateMethod.Length == 0)
            Debug.Log ("Event trigger doesn't have method name");
    }
}
